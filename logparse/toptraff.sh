#!/bin/bash

awk '{a[$1]+=$10}END{for (i in a){print i,a[i]}}' $1 | sort -rhk2 | head -10
