#!/bin/bash

if [[ -e $1 ]]
    then
        cut -d' ' -f1 $1 | sort | uniq -c | sort -n | tail -1
fi
