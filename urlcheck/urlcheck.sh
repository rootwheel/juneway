#!/bin/bash


function urlcheck ()
if [[ -e $1 ]]
    then
    while read LINE
        do
            CHECKCODE=$(curl -s -o /dev/null -w '%{http_code}' $LINE)
            if [[ ( $CHECKCODE -ge 100) && ($CHECKCODE -le 399) ]]
                 then
                    printf '%s\n' "Link OK. HTTP code: $CHECKCODE"
                 else
                    printf '%s\n' "Link FAIL. HTTP code: $CHECKCODE"
                 exit 1
            fi
    done < $1
    else 
    printf '%s\n' "Incorrect ULR file"
fi
sed -i '/^$/d' $1
urlcheck $1
